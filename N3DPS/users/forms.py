from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from flask_login import current_user
from N3DPS.models import User_sql

class SignupForm(FlaskForm):
    fname = StringField('First Name',
                        validators=[DataRequired(), Length(min=2, max=20)])
    lname = StringField('Last Name',
                        validators=[DataRequired(), Length(min=2, max=20)])
    username = StringField('Username',
                            validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):
        user = User_sql.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('That username is taken. Please choose a different one.')

    def validate_email(self, email):
        user = User_sql.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('That email is already in use.')

class LoginForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')

class UpdateAccountForm(FlaskForm):
    fname = StringField('First Name',
                        validators=[DataRequired(), Length(min=2, max=20)])
    lname = StringField('Last Name',
                        validators=[DataRequired(), Length(min=2, max=20)])
    username = StringField('Username',
                            validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    picture = FileField('Update Profile Picture', validators=[FileAllowed(['jpg', 'png'])])
    address_line1 = StringField('Line 1',
                            validators=[DataRequired(), Length(min=2, max=50)])
    address_line2 = StringField('Line 2',
                            validators=[DataRequired(), Length(min=0, max=50)])
    address_town = StringField('Town',
                            validators=[DataRequired(), Length(min=2, max=50)])
    address_county = StringField('County',
                            validators=[DataRequired(), Length(min=2, max=30)])
    address_country = StringField('Country',
                            validators=[DataRequired(), Length(min=2, max=30)])
    address_pcode = StringField('Postcode',
                            validators=[DataRequired(), Length(min=2, max=7)])
    submit = SubmitField('Update')

    def validate_username(self, username):
        if username.data != current_user.username:
            user = User_sql.query.filter_by(username=username.data).first()
            if user:
                raise ValidationError('That username is taken. Please choose a different one.')

    def validate_email(self, email):
        if email.data != current_user.email:
            user = User_sql.query.filter_by(email=email.data).first()
            if user:
                raise ValidationError('That email is already in use.')

class RequestResetForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')

    def validate_email(self, email):
        user = User_sql.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('There is no account with that email. You must register first.')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')