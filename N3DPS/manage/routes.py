from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import current_user, login_required
# from N3DPS.manage.forms import AddProductForm

manage = Blueprint('manage',__name__)

@manage.route("/admin")
@login_required
def admin():
    if current_user.level >= 2:
        return render_template('admin_panel.html', title='Admin Panel|N3DPS')
    else:
        return redirect(url_for('users.login'))

@manage.route("/admindonate")
@login_required
def admin_donate():
    if current_user.level >= 3:
        return render_template('admin_donate.html', title='Admin Donate View|N3DPS')
    else:
        return redirect(url_for('users.login'))

@manage.route("/adminmanageproducts")
@login_required
def manageproduct():
    if current_user.level >= 4:
        return render_template('manage_product.html', title='Admin Manage Product|N3DPS')
    else:
        return redirect(url_for('users.login'))

@manage.route("/adminaddproducts")
@login_required
def addproduct():
    if current_user.level >= 4:
        # form = AddProductForm()
        # if form.validate_on_submit():
        #     flash('your')
        return render_template('add_product.html', title='Admin Add Product|N3DPS')
    else:
        return redirect(url_for('users.login'))