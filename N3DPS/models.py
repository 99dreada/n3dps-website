from flask import current_app
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from N3DPS import db, login_manager
from flask_login import UserMixin

@login_manager.user_loader
def load_user(user_id):
    return User_sql.query.get(int(user_id))

class User_sql(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    level = db.Column(db.Integer, nullable=False , default=0)
    fname = db.Column(db.String(20), nullable=False)
    lname = db.Column(db.String(20), nullable=False)
    username = db.Column(db.String(20), unique=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default="default.png")
    password = db.Column(db.String(60), nullable=False)
    address_line1 = db.Column(db.String(20))
    address_line2 = db.Column(db.String(20))
    address_town = db.Column(db.String(20))
    address_county = db.Column(db.String(20))
    address_country = db.Column(db.String(30))
    address_pcode = db.Column(db.String(7))

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User_sql.query.get(user_id)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"

class Our_Stock_sql(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pname = db.Column(db.String(20), nullable=False)
    discription = db.Column(db.String(120), nullable=False)
    value = db.Column(db.Integer)
    image_file = db.Column(db.String(20), nullable=False, default="default.png")
    quantity = db.Column(db.Integer)

class Products(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pname = db.Column(db.String(20), nullable=False)
    discription = db.Column(db.String(120), nullable=False)
    value = db.Column(db.Integer)
    image_file = db.Column(db.String(20), nullable=False, default="default_product.png")


class in_orders_sql(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    acc_id = db.Column(db.Integer)
    pname = db.Column(db.String(20), nullable=False)
    quantity = db.Column(db.Integer)
    notes = db.Column(db.String(120), nullable=False)

class out_orders_sql(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    acc_id = db.Column(db.Integer)
    pname = db.Column(db.String(20), nullable=False)
    quantity = db.Column(db.Integer)
    notes = db.Column(db.String(120), nullable=False)