from flask import render_template, Blueprint
from flask_login import login_required

store = Blueprint('store',__name__)

@store.route("/redeem")
@login_required
def redeem():
    return render_template('redeem.html', title='Redeem|N3DPS')

@store.route("/donate")
@login_required
def donate():
    return render_template('Donate.html', title='Donate|N3DPS')